#!/bin/bash

# Folders
BASE=$(cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd);
CONFIG_DIR=$BASE'/config';
DIST_DIR=$BASE'/dist';
SCR_DIR=$BASE'/src';

# Version
if [[ -f  $BASE'/.semver' ]]; then

    SEMVER=$BASE'/.semver';
    MAJOR=":major:";
    MINOR=":minor:";
    PATCH=":patch:";

    while IFS= read -r LINE
    do
        if [[ "$LINE" == *"$MAJOR"* ]]; then
            MAJOR_STRING=${LINE#"$MAJOR "};
        fi
        if [[ "$LINE" == *"$MINOR"* ]]; then
            MINOR_STRING=${LINE#"$MINOR "};
        fi
        if [[ "$LINE" == *"$PATCH"* ]]; then
            PATCH_STRING=${LINE#"$PATCH "};
        fi

    done < "$SEMVER"
    VERSION=$MAJOR_STRING.$MINOR_STRING.$PATCH_STRING;
fi

# Vars
VARS=$(cat $CONFIG_DIR'/VARS.cfg');
VARS=${VARS//$'\n'/\\n};
COLOUR=$(cat $CONFIG_DIR'/COLOUR.cfg');
COLOUR=${COLOUR//$'\n'/\\n};

# Prompt user
read -p "Will build stand-alone scripts over the '$DIST_DIR' folder. Are you sure (N)? " -n 1 -r
printf "\n"
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    exit 1
fi

# Create dist folder if not exists.
if [[ ! -f "$DIST_DIR" ]]; then
    mkdir -p DIST_DIR;
fi

for folder in $SRC_DIR/*; do

    for file in $folder/*; do

        if [[ -f "$file" ]]; then

            tmp_folder=$DIST_DIR/${folder##*/};
            tmp_file=$tmp_folder/${file##*/}

            if [[ ! -f "$tmp_folder" ]]; then
                mkdir -p $tmp_folder
            fi

            cp $file $tmp_file
            sed -i "s|%VARS%|$VARS|g" $tmp_file
            sed -i "s|%COLOUR%|$COLOUR|g" $tmp_file
        fi
    done
done

printf "Done .. \n";
exit 0
