#!/bin/bash

# -----------------------------------------------------------------------------
# deb-cleaner.sh
# -----------------------------------------------------------------------------
# Remove rc config from packages previously deleted and clear orphaned packages,
# using apt-get, dpkg and deborphan.
# -----------------------------------------------------------------------------
# @autor        David Martín <hola@dmar.dev>
# @copyright    2009 - David Martín
# @version      1.0.0
# @package      shell-tools
# -----------------------------------------------------------------------------

%colour%

# ---

printf "${title}deb-cleaner.sh${nc} ${ver} - Remove orphaned packages and rc configs. \n"

# Checking if deborphan is installed.
printf "Checking if deborphan is installed ..... ";
if ! [ -x "$(command -v deborphan)" ]; then
    printf "${redbg}Error${nc}: Cant find deborphan. \n"
    exit 1
else
    printf "${green} OK!! ${nc} \n \n"
fi

# Show orphan packages to user
if [[ $(deborphan) ]]; then
    printf "${red}Orphaned packages: ${nc} \n"
    deborphan
fi

# Show rc packages.
if [[ $(dpkg --list | grep "^rc" | cut -d " " -f 3) ]]; then
    printf "${red}RC packages: ${nc} \n"
    dpkg --list | grep "^rc" | cut -d " " -f 3
    printf "\n"
fi

# If no packages found, exit.
if ! [[ $(deborphan) && $(dpkg --list | grep "^rc" | cut -d " " -f 3) ]]; then
    printf "No packages found. Nothing to do here ... \n"
    exit 0
fi

# Confirm it. 
read -p "This method is in no way perfect or even reliable, so beware when using this! Are you sure (N)? " -n 1 -r
printf "\n"
if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    exit 1
fi

# Check dpkg for rc packages and remove them
dpkg --list | grep "^rc" | cut -d " " -f 3 | xargs sudo dpkg --purge 2>/dev/null 
 
# Purge ophaned packages and autoremove
while [[ $(deborphan) ]]
do
    sudo apt-get purge $(deborphan)
    sudo apt-get --purge autoremove
done

if ! [[ $(deborphan) ]]; then
    printf "All orphaned packages removed. \n"
fi
exit 0
