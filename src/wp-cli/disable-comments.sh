#!/bin/bash

# -----------------------------------------------------------------------------
# Disable comments (wp-cli).
# -----------------------------------------------------------------------------
# Disable comments for every post.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    2022 - David Martín
# @version      1.0.0
# @package      shell-tools
# -----------------------------------------------------------------------------

wp option update default_comment_status ""
wp post list --comment_status=open --format=ids | xargs wp post update --comment_status=closed
