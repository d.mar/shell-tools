#!/bin/bash

# -----------------------------------------------------------------------------
# Disable pings (wp-cli).
# -----------------------------------------------------------------------------
# Disable pings for every post.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    2022 - David Martín
# @version      1.0.0
# @package      shell-tools
# -----------------------------------------------------------------------------

wp option update default_pingback_flag ""
wp option update default_ping_status ""
wp post list --ping_status=open --format=ids | xargs wp post update --ping_status=closed
