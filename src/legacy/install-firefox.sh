#!/bin/bash

TMP_DIR=`mktemp --directory`

MOZ_BASE_URL='http://ftp.mozilla.org/pub/mozilla.org'
MOZ_APP='firefox'

ARCH=${ARCH:-$(uname -m)}
LANG='es-ES'
#VERSION=${VERSION:-$(wget -qO- $MOZ_BASE_URL/$MOZ_APP/releases/latest/linux-$ARCH/$LANG/ | sed -nr 's|.*>$MOZ_APP-(.*)\.tar\.bz2</a>.*|\1|p')}
VERSION=${VERSION:-$(wget -qO- http://ftp.mozilla.org/pub/mozilla.org/firefox/releases/latest/linux-x86_64/es-ES/ | sed -nr 's|.*>firefox-(.*)\.tar\.bz2</a>.*|\1|p')}

echo 'Instalando Firefox versión '$VERSION'en /opt/firefox ..'

if [ -z $VERSION ]; then
    echo 'Error: No se ha podido determinar la última versión de '$MOZ_APP >&2
    exit 1
fi

#wget $MOZ_BASE_URL/$MOZ_APP/releases/latest/linux-$ARCH/$LANG/$MOZ_APP-$VERSION.tar.bz2 -P $TMP_DIR
wget http://ftp.mozilla.org/pub/mozilla.org/firefox/releases/latest/linux-x86_64/es-ES/firefox-$VERSION.tar.bz2 -P $TMP_DIR

#tar -jxvf $TMP_DIR/$MOZ_APP-$VERSION.tar.bz2 -C $TMP_DIR
tar -jxvf $TMP_DIR/firefox-$VERSION.tar.bz2 -C $TMP_DIR

if [ -d '/opt/firefox' ]; then
    rm -rf /opt/firefox
fi

mv $TMP_DIR/firefox /opt/firefox
chown root:root -R /opt/firefox
chmod 755 -R /opt/firefox

update-alternatives --install /usr/bin/firefox firefox /opt/firefox/firefox 100

