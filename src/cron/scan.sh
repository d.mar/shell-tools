#!/bin/bash

# -----------------------------------------------------------------------------
# Scan task with clamscan.
# -----------------------------------------------------------------------------
# Simple file system scan with clam, reporting to mail.
# -----------------------------------------------------------------------------
#
# @autor        David Martín <hola@dmar.dev>
# @copyright    2019 - David Martín
# @version      1.0.0
# @package      shell-tools
# -----------------------------------------------------------------------------

fqdn="$(hostname --fqdn)";
date="$(date)";
folder_to_scan="";
scan_output="";

# ---

report_to="";
report_from="From:root@$fqdn";
report_subject="$fqdn - Infected files.";
report_msg="Infected files has been found at $fqdn: ";
report_headers="Content-Type: text/plain; charset=utf-8";
report_bcc="Bcc:";

# Check if clamscan is available.
if ! [ -x "$(command -v clamscan)" ]; then  
  exit 1
fi

# Do it!
scan_output="$(clamscan -ri $folder_to_scan)";

# Report it!
if [[ $? -eq 1 ]]; then
  echo -e "$report_msg\n\r$scan_output\n\r" | mail -s "$report_subject" -a"$report_headers" -a"$report_from" -a"$report_bcc" "$report_to"
fi

exit 0